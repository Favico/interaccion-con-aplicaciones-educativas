# Importar librerias
from opensimplex import OpenSimplex
import numpy as np
import bge
from random import randint

# Guardar la escena actual
scene = bge.logic.getCurrentScene()

# Crear el objeto con la libreria opensimplex y definimos alentoriamente  sus dimensiones.
seed_altura = randint(0, 800)
seed_humedad = randint(0, 800)
gen_altura = OpenSimplex(seed=seed_altura)
gen_humedad = OpenSimplex(seed=seed_humedad)

# Dimensiones del mapa
alto = 30
ancho = 30

# Array para guardar la elevacion
mapa_altura = np.empty([alto, ancho])
mapa_humedad = np.empty([alto, ancho])

# Valor de la frecuencia
# Zoom de la camara
freq = 2

# Inicializar la variable de la altura
altura = 0

# Llenar los objetos
for y in range(alto):
    for x in range(ancho):
        # Dividir la posicion por la altura
        nx = 2 * x / ancho
        ny = 2 * y / alto

        # Generar el valor de la posicion actual
        mapa_altura[y][x] = gen_altura.noise2d(freq * nx, freq * ny) + 0.5 * gen_altura.noise2d(4 * freq * nx,
                                                                                                4 * freq * ny) + 0.25 * gen_altura.noise2d(
            8 * freq * nx, 8 * freq * ny)

        # La humedad la escalaremos para que el rango vaya de 0 a 1 en vez de -1 a 1
        mapa_humedad[y][x] = (gen_humedad.noise2d(freq * nx, freq * ny) + 0.5 * gen_humedad.noise2d(4 * freq * nx,
                                                                                                    4 * freq * ny) + 0.25 * gen_humedad.noise2d(
            8 * freq * nx, 8 * freq * ny)) / 2.0 + 0.5

# Dibujar el mapa
for j in range(len(mapa_altura)):
    for i in range(len(mapa_altura[j])):

        # Mar
        if mapa_altura[j][i] < 0:
            newobject = scene.addObject('Mar', 'Mar')
            altura = 0

        #   Bosque tropical el cual se gnerara a partir de de la sentencia if
        elif mapa_altura[j][i] < 0.25:
            if mapa_humedad[j][i] > 0.5:
                newobject = scene.addObject('Bosque_tropical', 'Bosque_tropical')
            elif mapa_humedad[j][i] > 0.25:
                newobject = scene.addObject('Praderia', 'Praderia')
            else:
                newobject = scene.addObject('Desierto_tropical', 'Desierto_tropical')
            altura = 0

        # Elevacion media
        elif mapa_altura[j][i] < 0.5:
            if mapa_humedad[j][i] > 0.55:
                newobject = scene.addObject('Bosque_templado', 'Bosque_templado')
            elif mapa_humedad[j][i] > 0.25:
                newobject = scene.addObject('Praderia_templada', 'Praderia_templada')
            else:
                newobject = scene.addObject('Desierto_templado', 'Desierto_templado')
            altura = 0.4

        # Elevacion alta
        elif mapa_altura[j][i] < 0.75:
            if mapa_humedad[j][i] > 0.66:
                newobject = scene.addObject('Taiga', 'Taiga')
            elif mapa_humedad[j][i] > 0.33:
                newobject = scene.addObject('Matorrales', 'Matorrales')
            else:
                newobject = scene.addObject('Desierto_frio', 'Desierto_frio')
            altura = 0.8

        # Elevacion Altas solo se creara bosques.
        else:
            newobject = scene.addObject('Bosque_templado')
            altura = 1.2

        # Se llama al nuevo objeto y se lo ubica en la ecena
        newobject.worldPosition = [i * 2, j * 2, altura]